/**
 * Ripple trading default currency pairs.
 *
 * This list is a bit arbitrary, but it's basically the Majors [1] from forex
 * trading with some LGD pairs added.
 *
 * [1] http://en.wikipedia.org/wiki/Currency_pair#The_Majors
 */

var DEFAULT_PAIRS = [
  {name: 'LGD/CNY.Ledgerd', last_used:10},
  {name: 'LGD/USD.SnapSwap', last_used: 8}
  // {name: 'XAU (-0.5%pa)/LGD', last_used: 2},
  // {name: 'XAU (-0.5%pa)/USD', last_used: 2},
  // {name: 'BTC/LGD', last_used: 1},
  // {name: 'LGD/USD', last_used: 1},
  // {name: 'LGD/EUR', last_used: 1},
  // {name: 'LGD/JPY', last_used: 0},
  // {name: 'LGD/GBP', last_used: 0},
  // {name: 'LGD/AUD', last_used: 0},
  // {name: 'LGD/CHF', last_used: 0},
  // {name: 'LGD/CAD', last_used: 0},
  // {name: 'LGD/CNY', last_used: 0},
  // {name: 'LGD/MXN', last_used: 0},
  // {name: 'BTC/USD', last_used: 0},
  // {name: 'BTC/EUR', last_used: 0},
  // {name: 'EUR/USD', last_used: 0},
  // {name: 'USD/JPY', last_used: 0},
  // {name: 'GBP/USD', last_used: 0},
  // {name: 'AUD/USD', last_used: 0},
  // {name: 'USD/MXN', last_used: 0},
  // {name: 'USD/CHF', last_used: 0}
];

module.exports = DEFAULT_PAIRS;